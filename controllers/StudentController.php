<?php

namespace app\controllers;

use Yii;

use yii\web\Controller;
use app\models\Student;
use yii\db\ActiveRecord;


class StudentController extends Controller
{
   
   public function actionView($id)
    {
		//echo "Student controller works";
		$name = Student::getName($id); // קריאה לפונקציה סטטית
		$IDnumber = Student::getIDnumber($id);
		$age = Student::getAge($id);
       return $this->render('view',['name' => $name ,'IDnumber' => $IDnumber ,'age'=> $age]); // הפונקציה render אחראית על העברת המידע ל view
    }
	
		public function actionIndex()
	    {
	    	$student = Student::find()			    
			    ->one();
	        return $this->render('index', ['student' => $student]);
	    }
		
		public function actionShow()
			    {
	    	$student = Student::getAll();
	        return $this->render('show', ['student' => $student]);
	    }
		
	
}